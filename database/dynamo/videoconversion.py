import logging
from pathlib import Path, PureWindowsPath #pour les traitement de fichier via python
import ffmpy
import time
import os
import json
import ssl
import boto3 # boto3 pour AWS
import botocore # pour les exceptions

class VideoConversion(object):
    # Constructor
    def __init__(self, _config_):

        self.config = _config_

        self.amazonBDD = boto3.resource('dynamodb',region_name='eu-west-3',aws_access_key_id=_config_.getAmazonCle(),aws_secret_access_key=_config_.getAmazonCleSecrete())
        self.table = self.amazonBDD.Table(_config_.getAmazonTable())


        self.amazonS3 = boto3.resource('s3',region_name='eu-west-3',aws_access_key_id=_config_.getAmazonCle(),aws_secret_access_key=_config_.getAmazonCleSecrete())
        self.compartiment = self.amazonS3.Bucket(_config_.getAmazonCompartiment())

    def convert(self, _id_, _uri_):
        logging.info('convert' + _uri_)

        
        filename = PureWindowsPath(self.config.getComputerChemin()+_uri_)
        correct_path = Path(filename)
        
        # le fichier n'existe pas donc je le télécharge depuis le s3
        if not correct_path.is_file():
            print('Download du fichier')
            self.compartiment.download_file(_uri_, self.config.getComputerChemin()+'copy_'+_uri_)


        # Je modifie le nom du fichier pour le test. Je n'utilise pas ffmpeg parce que ce n'est pas forcément le but de l'exercice
        #os.rename('C:\\Users\\farou\\Documents\\Giraud\\worker\\'+_uri_ , 'C:\\Users\\farou\\Documents\\Giraud\\worker\\copy_'+_uri_)

        # vérification si le fichier existe dans le s3 avant de l'upload
        try:
            self.compartiment.Object('copy_'+_uri_).load()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == '404':
                # le fichier n'existe pas sur le S3
                # je l'upload dans le s3
                self.compartiment.upload_file(self.config.getComputerChemin()+'copy_'+_uri_, 'copy_'+_uri_)
            else:
                print('Fichier déjà présent dans le 3')
                
		# mise à jour de la bdd
        self.table.update_item(
            Key={
                'convert':_id_
            },
            UpdateExpression='SET converted = :valuesToAdd',
            ExpressionAttributeValues={
            ":valuesToAdd": 1,
            },
        )