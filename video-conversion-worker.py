#!/usr/bin/python3.5

import logging
import signal

from configuration.configuration import Configuration
from messaging.videoconversionmessaging import VideoConversionMessaging
from database.dynamo.videoconversion import VideoConversion
from videoconvunixsocket.videoconversionunixsocket import VideoConversionUnixSocket

# Application entry point
if __name__ == '__main__':
    configuration = Configuration()

    video_unix_socket = VideoConversionUnixSocket()
    video_unix_socket.start()
    video_conversion_service = VideoConversion(configuration)
    video_messaging = VideoConversionMessaging(configuration, video_conversion_service)
    video_unix_socket.setVideoConversionMessaging(video_messaging)