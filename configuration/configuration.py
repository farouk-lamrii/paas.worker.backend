import yaml
import logging


# Clasic config class 
class Configuration(object):
    def __init__(self):
        self.configuration_file = 'C:\\Users\\farou\\Documents\\Giraud\\worker\\application.yml'
        self.configuration_data = None

        f = open(self.configuration_file, 'r')
        self.configuration_data = yaml.load(f.read())
        f.close()

    # récupération des infos importantes pour la connexion à AWS et le nom de la table
    def getAmazonCompartiment(self):
        return self.configuration_data['aws']['compartiment']

    def getAmazonCle(self):
        return self.configuration_data['aws']['cle']
		
    def getAmazonCleSecrete(self):
        return self.configuration_data['aws']['cleSecrete']

    def getAmazonTable(self):
        return self.configuration_data['aws']['table']


     # récupération des infos importantes concernant GPUBSUB 
    def getPubSubProjet(self):
        return self.configuration_data['pubsub']['projet']

    def getPubSubAbonnement(self):
        return self.configuration_data['pubsub']['abonnement']
		
    def getPubSubSujet(self):
        return self.configuration_data['pubsub']['sujet']

    # récupération du chemin où je vais travailler (ex: stocker le fichier téléchargé depuis le s3)
    def getComputerChemin(self):
        return self.configuration_data['ordinateur']['chemin']