from threading import Thread
import logging
import json
import time
import queue
from google.cloud import pubsub_v1


class VideoConversionMessaging(Thread):
	# Constructor
    def __init__(self, _config_, converting_service):
        Thread.__init__(self)
		
        self.converting_service = converting_service
		
        self.subscriber = pubsub_v1.SubscriberClient()
        self.subscription_path = self.subscriber.subscription_path(_config_.getPubSubProjet(), _config_.getPubSubAbonnement())
        self.start()

	   #This method will be called when an element is added in the pub sub
        def callback(message):
            print('Received message: {}'.format(message))
            self._on_message_(message.data)
            message.ack()

        self.subscriber.subscribe(self.subscription_path, callback=callback)
        

    def run(self):
        while True :
            print('Listening for messages')
            time.sleep(30) # écoute les messages toutes les x secondes


    def on_message(self, channel, method_frame, header_frame, body):
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["identifiant"], convert_request['Nom Du fichier'])

    def _on_message_(self,  body):
        convert_request = json.loads(body.decode())
        logging.info(body)
        self.converting_service.convert(convert_request["identifiant"], convert_request['Nom Du fichier'])